angular.module('app.routes', [])

.config(function($stateProvider, $urlRouterProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider
    

      .state('signup', {
    url: '/page2',
    templateUrl: 'templates/signup.html',
    controller: 'signupCtrl'
  })

  .state('login', {
    url: '/page3',
    templateUrl: 'templates/login.html',
    controller: 'loginCtrl'
  })

  .state('loginP', {
    url: '/page52',
    templateUrl: 'templates/loginP.html',
    controller: 'loginPCtrl'
  })

  .state('front', {
    url: '/page53',
    templateUrl: 'templates/front.html',
    controller: 'frontCtrl'
  })

  .state('homeP', {
    url: '/page39',
    templateUrl: 'templates/homeP.html',
    controller: 'homePCtrl'
  })

  .state('home', {
    url: '/page38',
    templateUrl: 'templates/home.html',
    controller: 'homeCtrl'
  })

  .state('profile', {
    url: '/page34',
    templateUrl: 'templates/profile.html',
    controller: 'profileCtrl'
  })

  .state('profileP', {
    url: '/page41',
    templateUrl: 'templates/profileP.html',
    controller: 'profilePCtrl'
  })

  .state('points', {
    url: '/page37',
    templateUrl: 'templates/points.html',
    controller: 'pointsCtrl'
  })

  .state('pointsP', {
    url: '/page42',
    templateUrl: 'templates/pointsP.html',
    controller: 'pointsPCtrl'
  })

  .state('help', {
    url: '/page36',
    templateUrl: 'templates/help.html',
    controller: 'helpCtrl'
  })

  .state('helpP', {
    url: '/page51',
    templateUrl: 'templates/helpP.html',
    controller: 'helpPCtrl'
  })

  .state('category', {
    url: '/page22',
    templateUrl: 'templates/category.html',
    controller: 'categoryCtrl'
  })

  .state('categoryP', {
    url: '/page48',
    templateUrl: 'templates/categoryP.html',
    controller: 'categoryPCtrl'
  })

  .state('favorites', {
    url: '/page16',
    templateUrl: 'templates/favorites.html',
    controller: 'favoritesCtrl'
  })

  .state('search', {
    url: '/page44',
    templateUrl: 'templates/search.html',
    controller: 'searchCtrl'
  })

  .state('cart', {
    url: '/page32',
    templateUrl: 'templates/cart.html',
    controller: 'cartCtrl'
  })

  .state('cartP', {
    url: '/page40',
    templateUrl: 'templates/cartP.html',
    controller: 'cartPCtrl'
  })

  .state('scan', {
    url: '/page45',
    templateUrl: 'templates/scan.html',
    controller: 'scanCtrl'
  })

  .state('clientsP', {
    url: '/page43',
    templateUrl: 'templates/clientsP.html',
    controller: 'clientsPCtrl'
  })

  .state('settings', {
    url: '/page35',
    templateUrl: 'templates/settings.html',
    controller: 'settingsCtrl'
  })

  .state('settingsP', {
    url: '/page49',
    templateUrl: 'templates/settingsP.html',
    controller: 'settingsPCtrl'
  })

  .state('checkout', {
    url: '/page33',
    templateUrl: 'templates/checkout.html',
    controller: 'checkoutCtrl'
  })

  .state('product', {
    url: '/page15',
    templateUrl: 'templates/product.html',
    controller: 'productCtrl'
  })

  .state('productP', {
    url: '/page47',
    templateUrl: 'templates/productP.html',
    controller: 'productPCtrl'
  })

  .state('thankYouPurchase', {
    url: '/page12',
    templateUrl: 'templates/thankYouPurchase.html',
    controller: 'thankYouPurchaseCtrl'
  })

  .state('scanSuccessful', {
    url: '/page46',
    templateUrl: 'templates/scanSuccessful.html',
    controller: 'scanSuccessfulCtrl'
  })

  .state('thankYouPurchaseP', {
    url: '/page30',
    templateUrl: 'templates/thankYouPurchaseP.html',
    controller: 'thankYouPurchasePCtrl'
  })

  .state('thankYouMessageSent', {
    url: '/page13',
    templateUrl: 'templates/thankYouMessageSent.html',
    controller: 'thankYouMessageSentCtrl'
  })

  .state('thankYouMessageSentP', {
    url: '/page50',
    templateUrl: 'templates/thankYouMessageSentP.html',
    controller: 'thankYouMessageSentPCtrl'
  })

$urlRouterProvider.otherwise('/page53')


});