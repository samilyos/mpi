angular.module('app.controllers', [])
  
.controller('signupCtrl', ['$scope', '$state', '$http', '$interval', '$ionicLoading', '$ionicScrollDelegate', '$ionicPopup', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $rootScope, $ionicModal, $ionicSlideBoxDelegate,
                                     $ionicTabsDelegate, $ionicLoading,
                                     $ionicPopup, $timeout, $state,
                                     $ionicSideMenuDelegate, $translate,
                                     $ionicPlatform, $ionicHistory) {


$ionicPlatform.registerBackButtonAction(function () {
            if ($ionicHistory.currentStateName() === 'app.home') {
                $scope.showExit();
            } else {
                navigator.app.backHistory();
            }
        }, 100);


}])
   
.controller('loginCtrl', ['$scope', '$state', '$http', '$interval', '$ionicLoading', '$ionicScrollDelegate', '$ionicPopup', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $rootScope, $ionicModal, $ionicSlideBoxDelegate,
                                     $ionicTabsDelegate, $ionicLoading,
                                     $ionicPopup, $timeout, $state,
                                     $ionicSideMenuDelegate, $translate,
                                     $ionicPlatform, $ionicHistory) {

 $scope.showLogin = function () {
            $scope.loginData = {};
            if (Config.getRememberme()) {
                $scope.loginData.rememberme = true;
                $scope.loginData.username = Config.getUsername();
                $scope.loginData.password = Config.getPassword();
            }

            var popupLogin = $ionicPopup.show({
                templateUrl: 'templates/login.html',
                title: $scope.translations.login_title,
                cssClass: 'login-container',
                scope: $scope,
                buttons: [
                    {text: $scope.translations.cancel},
                    {
                        text: $scope.translations.login,
                        type: 'button-assertive',
                        onTap: function (e) {
                            e.preventDefault();
                            if (!$scope.loginData.username || !$scope.loginData.password) {
                                return;
                            }

                            $scope.showLoading();
                            $rootScope.service.get('login', $scope.loginData, function (res) {
                                $scope.hideLoading();

                                if (res.code || res.message) {
                                    alert(res.message || res.code);
                                    return;
                                }
                                $scope.user = res;
                                Config.setRememberme($scope.loginData.rememberme);
                                if ($scope.loginData.rememberme) {
                                    Config.setUsername($scope.loginData.username);
                                    Config.setPassword($scope.loginData.password);
                                }
                                else{
                                    Config.setUsername('');
                                    Config.setPassword('');
                                }
                                $scope.hideLogin();
                            });
                        }
                    }
                ]
            });
            $scope.hideLogin = function () {
                popupLogin.close();
            };
        };
 
}])
   
.controller('loginPCtrl', ['$scope', '$state', '$http', '$interval', '$ionicLoading', '$ionicScrollDelegate', '$ionicPopup', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $rootScope, $ionicModal, $ionicSlideBoxDelegate,
                                     $ionicTabsDelegate, $ionicLoading,
                                     $ionicPopup, $timeout, $state,
                                     $ionicSideMenuDelegate, $translate,
                                     $ionicPlatform, $ionicHistory) {
$scope.showLogin = function () {
            $scope.loginData = {};
            if (Config.getRememberme()) {
                $scope.loginData.rememberme = true;
                $scope.loginData.username = Config.getUsername();
                $scope.loginData.password = Config.getPassword();
            }

            var popupLogin = $ionicPopup.show({
                templateUrl: 'templates/login.html',
                title: $scope.translations.login_title,
                cssClass: 'login-container',
                scope: $scope,
                buttons: [
                    {text: $scope.translations.cancel},
                    {
                        text: $scope.translations.login,
                        type: 'button-assertive',
                        onTap: function (e) {
                            e.preventDefault();
                            if (!$scope.loginData.username || !$scope.loginData.password) {
                                return;
                            }

                            $scope.showLoading();
                            $rootScope.service.get('login', $scope.loginData, function (res) {
                                $scope.hideLoading();

                                if (res.code || res.message) {
                                    alert(res.message || res.code);
                                    return;
                                }
                                $scope.user = res;
                                Config.setRememberme($scope.loginData.rememberme);
                                if ($scope.loginData.rememberme) {
                                    Config.setUsername($scope.loginData.username);
                                    Config.setPassword($scope.loginData.password);
                                }
                                else{
                                    Config.setUsername('');
                                    Config.setPassword('');
                                }
                                $scope.hideLogin();
                            });
                        }
                    }
                ]
            });
            $scope.hideLogin = function () {
                popupLogin.close();
            };
        };
  

}])
   
.controller('frontCtrl', ['$scope', '$state', '$http', '$interval', '$ionicLoading', '$ionicScrollDelegate', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $state, $http, $interval, $ionicLoading, $ionicScrollDelegate) {



}])
   
.controller('homePCtrl', ['$scope', '$state', '$http', '$interval', '$ionicLoading', '$ionicScrollDelegate', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $rootScope, $state, $ionicSlideBoxDelegate) {

$scope.searchData = {};

        $('.homebanner-swiper-container').height($(window).width() * 4 / 9);
        $scope.updateSlider = function () {
            $ionicSlideBoxDelegate.$getByHandle('image-viewer').update();
        };

        $scope.goFirst = function ($index) {
            if ($index == $ionicSlideBoxDelegate.$getByHandle('image-viewer').slidesCount()-1)  {
                setTimeout(function() {
                        $ionicSlideBoxDelegate.$getByHandle('image-viewer').slide(0);
                    },3000);
            }
        };

        $rootScope.service.get('getBannerBlock', {block:'app_home_banner1'}, function (results) {
            $scope.banner1 = results;

            var banner1 = [];
            for (var key in results) {
                banner1.push(results[key]);
            }
            $scope.banner1 = banner1;
        });

        $scope.onSearch = function () {
            if (!$scope.searchData.text) {
                return;
            }
            $rootScope.search = {
                type: 'search',
                params: {
                    q: $scope.searchData.text
                }
            };
            $state.go('app.searchResult');
        };
  

}])
   
.controller('homeCtrl', ['$scope', '$state', '$http', '$interval', '$ionicLoading', '$ionicScrollDelegate', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $rootScope, $state, $ionicSlideBoxDelegate) {

$scope.searchData = {};

        $('.homebanner-swiper-container').height($(window).width() * 4 / 9);
        $scope.updateSlider = function () {
            $ionicSlideBoxDelegate.$getByHandle('image-viewer').update();
        };

        $scope.goFirst = function ($index) {
            if ($index == $ionicSlideBoxDelegate.$getByHandle('image-viewer').slidesCount()-1)  {
                setTimeout(function() {
                        $ionicSlideBoxDelegate.$getByHandle('image-viewer').slide(0);
                    },3000);
            }
        };

        $rootScope.service.get('getBannerBlock', {block:'app_home_banner1'}, function (results) {
            $scope.banner1 = results;

            var banner1 = [];
            for (var key in results) {
                banner1.push(results[key]);
            }
            $scope.banner1 = banner1;
        });

        $scope.onSearch = function () {
            if (!$scope.searchData.text) {
                return;
            }
            $rootScope.search = {
                type: 'search',
                params: {
                    q: $scope.searchData.text
                }
            };
            $state.go('app.searchResult');
        };
  
}])
   
.controller('profileCtrl', ['$scope', '$state', '$http', '$interval', '$ionicLoading', '$ionicScrollDelegate', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $rootScope,
                                     $ionicModal, $ionicSlideBoxDelegate,
                                     $ionicTabsDelegate, $ionicLoading,
                                     $ionicPopup, $timeout, $state,
                                     $ionicSideMenuDelegate, $translate,
                                     $ionicPlatform, $ionicHistory) {

$scope.getUser = function () {
            $rootScope.service.get('user', function (user) {
                $scope.user = typeof user === 'object' ? user : null;
            });
        };
        $scope.getUser();
        if (!$scope.user) {
            $scope.autoLogin();
        };

}])
   
.controller('profilePCtrl', ['$scope', '$state', '$http', '$interval', '$ionicLoading', '$ionicScrollDelegate', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $rootScope,
                                     $ionicModal, $ionicSlideBoxDelegate,
                                     $ionicTabsDelegate, $ionicLoading,
                                     $ionicPopup, $timeout, $state,
                                     $ionicSideMenuDelegate, $translate,
                                     $ionicPlatform, $ionicHistory) {

$scope.getUser = function () {
            $rootScope.service.get('user', function (user) {
                $scope.user = typeof user === 'object' ? user : null;
            });
        };
        $scope.getUser();
        if (!$scope.user) {
            $scope.autoLogin();
        };

}])
   
.controller('pointsCtrl', ['$scope', '$state', '$http', '$interval', '$ionicLoading', '$ionicScrollDelegate', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $state, $http, $interval, $ionicLoading, $ionicScrollDelegate) {



}])
   
.controller('pointsPCtrl', ['$scope', '$state', '$http', '$interval', '$ionicLoading', '$ionicScrollDelegate', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $state, $http, $interval, $ionicLoading, $ionicScrollDelegate) {



}])
   
.controller('helpCtrl', ['$scope', '$state', '$http', '$interval', '$ionicLoading', '$ionicScrollDelegate', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $state, $http, $interval, $ionicLoading, $ionicScrollDelegate) {



}])
   
.controller('helpPCtrl', ['$scope', '$state', '$http', '$interval', '$ionicLoading', '$ionicScrollDelegate', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $state, $http, $interval, $ionicLoading, $ionicScrollDelegate) {



}])
   
.controller('categoryCtrl', ['$scope', '$state', '$http', '$interval', '$ionicLoading', '$ionicScrollDelegate', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $state, $http, $interval, $ionicLoading, $ionicScrollDelegate) {

var getList = function (func, callback) {
            if (func === 'load') {
                $scope.listPge++;
            } else {
                $scope.listPge = 1;
            }

            var params = {
                limit: 20,
                page: $scope.listPge,
                cmd: $stateParams.cmd || 'catalog'
            };
          }

}])
   
.controller('categoryPCtrl', ['$scope', '$state', '$http', '$interval', '$ionicLoading', '$ionicScrollDelegate', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $state, $http, $interval, $ionicLoading, $ionicScrollDelegate) {

    var getList = function (func, callback) {
            if (func === 'load') {
                $scope.listPge++;
            } else {
                $scope.listPge = 1;
            }

            var params = {
                limit: 20,
                page: $scope.listPge,
                cmd: $stateParams.cmd || 'catalog'
            };
          }

}])
   
.controller('favoritesCtrl', ['$scope', '$state', '$http', '$interval', '$ionicLoading', '$ionicScrollDelegate', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $state, $http, $interval, $ionicLoading, $ionicScrollDelegate) {



}])
   
.controller('searchCtrl', ['$scope', '$state', '$http', '$interval', '$ionicLoading', '$ionicScrollDelegate', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $state, $http, $interval, $ionicLoading, $ionicScrollDelegate) {

$rootScope.service.get('searchAdvField', {}, function (results) {
            var fields = [];

            for (var key in results) {
                fields.push(results[key]);
            }
            $scope.searchFields = fields;
        });
    

}])
   
.controller('cartCtrl', ['$scope', '$state', '$http', '$interval', '$ionicLoading', '$ionicScrollDelegate', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $state, $http, $interval, $ionicLoading, $ionicScrollDelegate) {

$rootScope.service.get('cartGetQty', {
            product: $stateParams.productid
        }, function (res) {
            $scope.items_qty = res.items_qty;
        });

}])
   
.controller('cartPCtrl', ['$scope', '$state', '$http', '$interval', '$ionicLoading', '$ionicScrollDelegate', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $state, $http, $interval, $ionicLoading, $ionicScrollDelegate) {

$rootScope.service.get('cartGetQty', {
            product: $stateParams.productid
        }, function (res) {
            $scope.items_qty = res.items_qty;
        });

}])
   
.controller('scanCtrl', ['$scope', '$state', '$http', '$interval', '$ionicLoading', '$ionicScrollDelegate', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $state, $http, $interval, $ionicLoading, $ionicScrollDelegate) {



}])
   
.controller('clientsPCtrl', ['$scope', '$state', '$http', '$interval', '$ionicLoading', '$ionicScrollDelegate', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $state, $http, $interval, $ionicLoading, $ionicScrollDelegate) {



}])
   
.controller('settingsCtrl', ['$scope', '$state', '$http', '$interval', '$ionicLoading', '$ionicScrollDelegate', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $state, $http, $interval, $ionicLoading, $ionicScrollDelegate) {



}])
   
.controller('settingsPCtrl', ['$scope', '$state', '$http', '$interval', '$ionicLoading', '$ionicScrollDelegate', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $state, $http, $interval, $ionicLoading, $ionicScrollDelegate) {



}])
   
.controller('checkoutCtrl', ['$scope', '$state', '$http', '$interval', '$ionicLoading', '$ionicScrollDelegate', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $state, $http, $interval, $ionicLoading, $ionicScrollDelegate) {



}])
   
.controller('productCtrl', ['$scope', '$state', '$http', '$interval', '$ionicLoading', '$ionicScrollDelegate', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $state, $http, $interval, $ionicLoading, $ionicScrollDelegate) {

 $scope.listTitle = {
            daily_sale: 'latest_promotions',
            'new': 'common_products',
            cert_download: 'cert_download'
        }[$stateParams.cmd];
        $scope.listPge = 1;
        $scope.hasInit = false;
        $scope.loadOver = false;
        if ($stateParams.cmd === 'daily_sale') {
            $scope.lineClass = 'one-line';
        }

        var getList = function (func, callback) {
            if (func === 'load') {
                $scope.listPge++;
            } else {
                $scope.listPge = 1;
            }

            var params = {
                limit: 20,
                page: $scope.listPge,
                cmd: $stateParams.cmd || 'catalog'
            };

            $scope.showLoading();
            $rootScope.service.get('products', params, function (lists) {
                if (func === 'load') {
                    if (Array.isArray(lists) && lists.length) {
                        $scope.lists = $scope.lists.concat(lists);
                    } else {
                        $scope.loadOver = true;
                    }
                } else {
                    $scope.hasInit = true;
                    $scope.lists = lists;
                    if (!localStorage['symbol']) {
                        localStorage['symbol'] = lists[0].symbol;
                    }
                }
                if (typeof callback === 'function') {
                    callback();
                }
            });

            $scope.hideLoading();
        };

        $scope.doRefresh = function () {
            getList('refresh', function () {
                $scope.$broadcast('scroll.refreshComplete');
            });
        };
        $scope.loadMore = function () {
            if (!$scope.hasInit || $scope.loadOver) {
                $scope.$broadcast('scroll.infiniteScrollComplete');
                return;
            }
            getList('load', function () {
                $scope.$broadcast('scroll.infiniteScrollComplete');
            });
        };

        getList('refresh');
    


}])
   
.controller('productPCtrl', ['$scope', '$state', '$http', '$interval', '$ionicLoading', '$ionicScrollDelegate', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $state, $http, $interval, $ionicLoading, $ionicScrollDelegate) {

 $scope.listTitle = {
            daily_sale: 'latest_promotions',
            'new': 'common_products',
            cert_download: 'cert_download'
        }[$stateParams.cmd];
        $scope.listPge = 1;
        $scope.hasInit = false;
        $scope.loadOver = false;
        if ($stateParams.cmd === 'daily_sale') {
            $scope.lineClass = 'one-line';
        }

        var getList = function (func, callback) {
            if (func === 'load') {
                $scope.listPge++;
            } else {
                $scope.listPge = 1;
            }

            var params = {
                limit: 20,
                page: $scope.listPge,
                cmd: $stateParams.cmd || 'catalog'
            };

            $scope.showLoading();
            $rootScope.service.get('products', params, function (lists) {
                if (func === 'load') {
                    if (Array.isArray(lists) && lists.length) {
                        $scope.lists = $scope.lists.concat(lists);
                    } else {
                        $scope.loadOver = true;
                    }
                } else {
                    $scope.hasInit = true;
                    $scope.lists = lists;
                    if (!localStorage['symbol']) {
                        localStorage['symbol'] = lists[0].symbol;
                    }
                }
                if (typeof callback === 'function') {
                    callback();
                }
            });

            $scope.hideLoading();
        };

        $scope.doRefresh = function () {
            getList('refresh', function () {
                $scope.$broadcast('scroll.refreshComplete');
            });
        };
        $scope.loadMore = function () {
            if (!$scope.hasInit || $scope.loadOver) {
                $scope.$broadcast('scroll.infiniteScrollComplete');
                return;
            }
            getList('load', function () {
                $scope.$broadcast('scroll.infiniteScrollComplete');
            });
        };

        getList('refresh');
    


}])
   
.controller('thankYouPurchaseCtrl', ['$scope', '$state', '$http', '$interval', '$ionicLoading', '$ionicScrollDelegate', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $state, $http, $interval, $ionicLoading, $ionicScrollDelegate) {



}])
   
.controller('scanSuccessfulCtrl', ['$scope', '$state', '$http', '$interval', '$ionicLoading', '$ionicScrollDelegate', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $state, $http, $interval, $ionicLoading, $ionicScrollDelegate) {



}])
   
.controller('thankYouPurchasePCtrl', ['$scope', '$state', '$http', '$interval', '$ionicLoading', '$ionicScrollDelegate', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $state, $http, $interval, $ionicLoading, $ionicScrollDelegate) {



}])
   
.controller('thankYouMessageSentCtrl', ['$scope', '$state', '$http', '$interval', '$ionicLoading', '$ionicScrollDelegate', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $state, $http, $interval, $ionicLoading, $ionicScrollDelegate) {



}])
   
.controller('thankYouMessageSentPCtrl', ['$scope', '$state', '$http', '$interval', '$ionicLoading', '$ionicScrollDelegate', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $state, $http, $interval, $ionicLoading, $ionicScrollDelegate) {



}])
 